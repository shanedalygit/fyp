//
//  perscription.m
//  rvm
//
//  Created by Shane Daly on 15/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "perscription.h"

@interface perscription ()
@end

// Core Data Objects
NSEntityDescription *presEntity;
NSManagedObjectContext *presObject;
NSArray *total_pres;
NSMutableArray *med_name, *dosage, *frequency, *perscription_id;

NSString *med = @"Name: ", *dose = @"Dosage";

@implementation perscription

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Initial Setup
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    presObject = [appd managedObjectContext];
    presEntity = [NSEntityDescription entityForName:@"Meds" inManagedObjectContext:presObject];
    
    [self populate];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) populate {
    
    // Get values where id ==
    NSFetchRequest *get_request = [[NSFetchRequest alloc] init];
    [get_request setEntity:presEntity];
    [get_request setReturnsObjectsAsFaults:NO];
    //[request setPredicate:predicate];
    NSError *error = nil;
    total_pres = [presObject executeFetchRequest:get_request error:&error];
    
    NSString *p_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"pat_id"];
    int o = (int)p_id;
    
    med_name = [total_pres valueForKey:@"name"];
    dosage = [total_pres valueForKey:@"dose"];
    frequency = [total_pres valueForKey:@"frequency"];
    perscription_id = [total_pres valueForKey:@"perscription"];
    
    
    // print the names and dosages
    for (int x = 0; x < [total_pres count]; x++) {
        
        if (o == (int)[perscription_id objectAtIndex:x]) {
            NSString *preName = [med_name objectAtIndex:x];
            NSString *preDose = [dosage objectAtIndex:x];
            NSString *preFreq = [frequency objectAtIndex:x];
            
            NSString *current = _totalPerscription.text;
            NSString *new = [NSString stringWithFormat:@"%@ - %@ - %@\r", preName, preDose, preFreq];
            _totalPerscription.text = [NSString stringWithFormat:@"%@ %@", current, new];
        }
        
    }
}

@end
