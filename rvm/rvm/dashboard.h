//
//  dashboard.h
//  rvm
//
//  Created by Shane Daly on 18/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
typedef enum patientState { to_discharge } State;

@interface dashboard : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
    AppDelegate *appd;
}
@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *administrator_settings;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *demo_data;

@end
