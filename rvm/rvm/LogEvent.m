//
//  LogEvent.m
//  rvm
//
//  Created by Shane Daly on 04/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "LogEvent.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface LogEvent ()

@end

@implementation LogEvent

NSArray *logDetails;
NSEntityDescription *log_Entity;
NSManagedObjectContext *logObject;
NSPredicate *logPredicate;
NSString *event_Pat;
int state = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initCoreData];
    // Do any additional setup after loading the view.
    self.note_Log.layer.borderWidth = 1.5f;
    self.note_Log.layer.borderColor = [[UIColor grayColor] CGColor];
    self.note_Log.text = @"Enter Note...";
    self.note_Log.textColor = [UIColor lightGrayColor];
    self.note_Log.delegate = self;
    
    self.event_Log.layer.borderWidth = 1.5f;
    self.event_Log.layer.borderColor = [[UIColor grayColor] CGColor];
    self.event_Log.text = @"Enter Event...";
    self.event_Log.textColor = [UIColor lightGrayColor];
    self.event_Log.delegate = self;
    
    NSString *the_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    _patientName_Log.text = the_name;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) initCoreData {
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    logObject = [appd managedObjectContext];
    log_Entity = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:logObject];
}

- (void) getCredentials {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:log_Entity];
    [request setReturnsObjectsAsFaults:NO];
    [request setPredicate:logPredicate];
    NSError *error = nil;
    logDetails = [logObject executeFetchRequest:request error:&error];
}

- (void) writeOut {
    
    NSEntityDescription *event_Descrip = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:logObject];
    NSFetchRequest *set_request = [[NSFetchRequest alloc] init];
    [set_request setEntity:event_Descrip];
    
    NSArray *totalEvents;
    NSError *error;
    NSManagedObject *event_;
    event_ = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext: logObject];
    totalEvents = [logObject executeFetchRequest:set_request error:&error];
    [logObject save:&error];
    
    // Get the event id.
    NSString *event_id = [NSString stringWithFormat:@"%ld", [totalEvents count]];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    event_Pat =    [defaults objectForKey:@"pat_id"];
    
    switch (state) {
        case 1:
            [event_ setValue:_note_Log.text forKey:@"event_descrip"];
            [event_ setValue:event_id forKey:@"event_id"];
            [event_ setValue:event_Pat forKey:@"patient_id"];
            break;
        case 2:
            [event_ setValue:_event_Log.text forKey:@"event_descrip"];
            [event_ setValue:event_id forKey:@"event_id"];
            [event_ setValue:event_Pat forKey:@"patient_id"];
            break;
        default:
            break;
    }
}

- (IBAction)submit_Note:(id)sender {
    state = 1;
    [self writeOut];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Note Added"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [self performSelector:@selector(finishedLog)];
                                                     }];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)submit_Event:(id)sender {
    state = 2;
    [self writeOut];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Event Logged"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [self performSelector:@selector(finishedLog)];
                                                     }];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) finishedLog {
    _note_Log.text = nil;
    _event_Log.text = nil;
}

- (void) textViewDidBeginEditing:(UITextView *) textView {
    [_note_Log setText:@""];
    [_event_Log setText:@""];
    self.event_Log.textColor = [UIColor blackColor];
    self.note_Log.textColor = [UIColor blackColor];
}
@end
