//
//  addUsers.h
//  rvm
//
//  Created by Shane Daly on 04/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface addUsers : UIViewController
{
    AppDelegate *appd;
}

@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITextField *thenew_username;
@property (weak, nonatomic) IBOutlet UITextField *thenew_password;
- (IBAction)create:(id)sender;
- (IBAction)iAgree:(id)sender;
- (IBAction)iDisagree:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addUserButton;
- (IBAction)return:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *staff_id;

@end
