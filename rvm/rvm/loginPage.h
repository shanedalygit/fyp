//
//  loginPage.h
//  rvm
//
//  Created by Shane Daly on 15/01/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface loginPage : UIViewController {
    AppDelegate *appd;
}

@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITextField *username_Field;
@property (weak, nonatomic) IBOutlet UITextField *password_Field;
@property (weak, nonatomic) IBOutlet UILabel *login_status;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)login:(id)sender;
- (IBAction)new_user:(id)sender;
- (IBAction)forgot_id:(id)sender;

@end
