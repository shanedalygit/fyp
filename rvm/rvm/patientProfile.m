//
//  patientProfile.m
//  rvm
//
//  Created by Shane Daly on 28/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import "patientProfile.h"
#import "AppDelegate.h"

@interface patientProfile ()

@end

NSArray *patient;
NSMutableArray *profile_details1, *room, *dob, *address1, *contact, *email1, *gpname, *gpcontact, *bed, *pat_id, *history, *kin, *kin_contact, *admission, *ad_reason;
NSEntityDescription *entityDesc;
NSManagedObjectContext *manObj;
NSPredicate *pred;
NSString *profile;

@implementation patientProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    _patientImage.layer.borderWidth = 1.0f;
    _patientImage.layer.masksToBounds = YES;
    self.history.layer.borderWidth = 1.5f;
    self.history.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    manObj = [appd managedObjectContext];
    entityDesc = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:manObj];

    // Get the Patients Name
    profile = [[NSUserDefaults standardUserDefaults] stringForKey:@"name"];
    [self initCoreData];
    [self populateProfile];
}

- (void) viewDidAppear:(BOOL)animated {
    [NSTimer scheduledTimerWithTimeInterval:2.0f
                                     target:self selector:@selector(checkReloads) userInfo:nil repeats:YES];
}

- (void) checkReloads {
    [self reloadInputViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initCoreData {
    pred = [NSPredicate predicateWithFormat:@"(name = %@)", profile];
}

- (void) populateProfile {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setReturnsObjectsAsFaults:NO];
    [request setPredicate:pred];
    NSError *error = nil;
    patient = [manObj executeFetchRequest:request error:&error];
    [self fillLabels];
   

}

- (void) fillLabels {
   
    room = [patient valueForKey:@"room"];
    pat_id = [patient valueForKey:@"id"];
    dob = [patient valueForKey:@"dob"];
    address1 = [patient valueForKey:@"address"];
    contact = [patient valueForKey:@"contact"];
    email1 = [patient valueForKey:@"email"];
    gpname = [patient valueForKey:@"gpname"];
    gpcontact = [patient valueForKey:@"gpcontact"];
    kin = [patient valueForKey:@"next_of_kin"];
    kin_contact = [patient valueForKey:@"next_kin_contact"];
    admission = [patient valueForKey:@"admit_date"];
    ad_reason = [patient valueForKey:@"admit_reason"];
    bed = [patient valueForKey:@"bed"];

    _name.text = profile;
    _room.text = [room objectAtIndex:0];
    _dob.text = [dob objectAtIndex:0];
    _address.text = [address1 objectAtIndex:0];
    _contact.text = [contact objectAtIndex:0];
    _email.text = [email1 objectAtIndex:0];
    _gpname.text = [gpname objectAtIndex:0];
    _gpcontact.text = [gpcontact objectAtIndex:0];
    _k_contact.text = [kin_contact objectAtIndex:0];
    _next_o_k.text = [kin objectAtIndex:0];
    _admission_date.text = [admission objectAtIndex:0];
    _admission_reason.text = [ad_reason objectAtIndex:0];
    _bed.text = [bed objectAtIndex:0];
    [self eventHistory];
}

- (void) eventHistory {
    
    
    NSFetchRequest *history_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *history_entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:manObj];
    NSPredicate *history_pred = [NSPredicate predicateWithFormat:@"(patient_id = %@)", [pat_id objectAtIndex:0]];
    [history_request setEntity:history_entity];
    [history_request setReturnsObjectsAsFaults:NO];
    [history_request setPredicate:history_pred];
    NSError *error = nil;
    NSArray *history_tab = [manObj executeFetchRequest:history_request error:&error];
    
    long size = [history_tab count];
    
    NSMutableArray *event_des = [history_tab valueForKey:@"event_descrip"];
    NSMutableArray *eventid = [history_tab valueForKey:@"event_id"];
    
    for (int x = 0; x < size; x++) {
        NSString *descEv = [event_des objectAtIndex:x];
        NSString *idEv = [eventid objectAtIndex:x];
        
        NSString *current = _history.text;
        NSString *append = [NSString stringWithFormat:@"%@ -- %@\r", idEv, descEv];
        _history.text = [NSString stringWithFormat:@"%@ %@", current, append];
    }
    
    if (!_history.hasText) {
        [_history setText:@"No History Present"];
        self.history.textColor = [UIColor lightGrayColor];
    }
}

- (IBAction)perscription_link:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:[pat_id objectAtIndex:0] forKey:@"pat_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)log_event:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:[pat_id objectAtIndex:0] forKey:@"pat_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) textViewDidBeginEditing:(UITextView *) textView {
    [_history setText:@""];
    self.history.textColor = [UIColor blackColor];
}
@end
