//
//  patientProfile.h
//  rvm
//
//  Created by Shane Daly on 28/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface patientProfile : UIViewController {
    AppDelegate *appd;
}
@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UIImageView *patientImage;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *room;
@property (weak, nonatomic) IBOutlet UILabel *dob;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *contact;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *gpname;
@property (weak, nonatomic) IBOutlet UILabel *gpcontact;
@property (weak, nonatomic) IBOutlet UILabel *bed;
- (IBAction)perscription_link:(id)sender;
- (IBAction)log_event:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *admission_date;
@property (weak, nonatomic) IBOutlet UILabel *admission_reason;
@property (weak, nonatomic) IBOutlet UITextView *history;
@property (weak, nonatomic) IBOutlet UILabel *next_o_k;
@property (weak, nonatomic) IBOutlet UILabel *k_contact;

@end
