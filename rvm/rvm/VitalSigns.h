//
//  VitalSigns.h
//  rvm
//
//  Created by Shane Daly on 09/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VitalSigns : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *hr;
- (IBAction)back:(id)sender;

@end
