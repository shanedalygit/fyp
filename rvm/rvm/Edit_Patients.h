//
//  Edit_Patients.h
//  rvm
//
//  Created by Shane Daly on 14/03/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Edit_Patients : UIViewController {
      AppDelegate *appd;
}
@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITextField *name_text;
@property (weak, nonatomic) IBOutlet UITextField *room_text;
@property (weak, nonatomic) IBOutlet UITextField *dob_text;
@property (weak, nonatomic) IBOutlet UITextField *address_text;
@property (weak, nonatomic) IBOutlet UITextField *contact_text;
@property (weak, nonatomic) IBOutlet UITextField *email_text;
@property (weak, nonatomic) IBOutlet UITextField *gp_names;
@property (weak, nonatomic) IBOutlet UITextField *gp_contacts;
@property (weak, nonatomic) IBOutlet UITextField *bed_text;
@property (weak, nonatomic) IBOutlet UITextField *next_of_kin_name;
@property (weak, nonatomic) IBOutlet UITextField *next_of_kin_no;
@property (weak, nonatomic) IBOutlet UITextView *history_text;
@property (weak, nonatomic) IBOutlet UILabel *admission_date;
@property (weak, nonatomic) IBOutlet UILabel *admission_reason;
- (IBAction)done:(id)sender;
- (IBAction)delete:(id)sender;


@end
