//
//  addMeds.h
//  rvm
//
//  Created by Shane Daly on 17/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface addMeds : UIViewController
{
    AppDelegate *appd;
}

@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITextField *medName;
@property (weak, nonatomic) IBOutlet UITextField *medDose;
@property (weak, nonatomic) IBOutlet UITextField *medFrequency;
- (IBAction)perAdd:(id)sender;
- (IBAction)cancel:(id)sender;

@end
