//
//  addMeds.m
//  rvm
//
//  Created by Shane Daly on 17/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "addMeds.h"

@interface addMeds ()

@end

// Core Data Objects
NSEntityDescription *medsEntity;
NSManagedObjectContext *medsObject;
NSArray *total_meds;

@implementation addMeds

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Initial Setup
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    medsObject = [appd managedObjectContext];
    medsEntity = [NSEntityDescription entityForName:@"Meds" inManagedObjectContext:medsObject];
    [self core];
}

- (void) core {
    
    // Get Perscription ID
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* persc_id =    [defaults objectForKey:@"pat_id"];
    
    // Get Values to get ID count.
    // --------------------------
    NSFetchRequest *get_request = [[NSFetchRequest alloc] init];
    [get_request setEntity:medsEntity];
    [get_request setReturnsObjectsAsFaults:NO];
    //[request setPredicate:predicate];
    NSError *error = nil;
    total_meds = [medsObject executeFetchRequest:get_request error:&error];
   
    // ID Count
    int med_id = (int)[total_meds count];
    int update = med_id +1;
    NSString *total = [NSString stringWithFormat:@"%d", update];
    
    // Writing Values to Core
    NSEntityDescription *medsDescrip = [NSEntityDescription entityForName:@"Meds" inManagedObjectContext:medsObject];
    NSFetchRequest *set_request = [[NSFetchRequest alloc] init];
    [set_request setEntity:medsDescrip];
    
    NSManagedObject *user;
    user = [NSEntityDescription insertNewObjectForEntityForName:@"Meds" inManagedObjectContext: medsObject];
    [medsObject save:&error];
    
    [user setValue:_medName.text forKey:@"name"];
    [user setValue:_medDose.text forKey:@"dose"];
    [user setValue:_medFrequency.text forKey:@"frequency"];
    [user setValue:total forKey:@"med_id"];
    [user setValue:persc_id forKey:@"perscription"];
    
    if([medsObject save:&error] ) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Medicine Added."
                                                                                 message:@"Would you like to add another item to the perscription?"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *yes = [UIAlertAction actionWithTitle:@"Yes"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             self->_medName.text = nil;
                                                             self->_medFrequency.text = nil;
                                                             self->_medDose.text = NULL;
                                                         }];
        
        UIAlertAction *no = [UIAlertAction actionWithTitle:@"No"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self performSelector:@selector(returnHome)];
                                                         }];
        [alertController addAction:yes];
        [alertController addAction:no];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void) returnHome {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)perAdd:(id)sender {
    
    [self core];
}

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
