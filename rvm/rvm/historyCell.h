//
//  historyCell.h
//  rvm
//
//  Created by Shane Daly on 05/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface historyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UITextView *description;

@end
