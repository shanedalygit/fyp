//
//  welcomeViewController.m
//  rvm
//
//  Created by Shane Daly on 17/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import "welcomeViewController.h"

@interface welcomeViewController ()

@end

@implementation welcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fadeText];
    [self fadeLogo];
    [self performSelector:@selector(transition) withObject:nil afterDelay:5.0];
}

- (void) fadeLogo {
    
    //Fade Image Out
    _logoFade.alpha = 0.0;
    CGRect aboutNicheImageFrame = _logoFade.frame;
    aboutNicheImageFrame.origin.y = self.view.bounds.size.height;

    [UIView animateWithDuration:0.75
                          delay:1.5
                        options:  UIViewAnimationOptionCurveEaseIn
                     animations:^{ self->_logoFade.alpha = 1; }
                     completion:^(BOOL finished) {
                         NSLog(@"Done!");
                     }];
}

- (void) fadeText {
    
    _welcomeLabel.alpha = 0.0;
    CGRect aboutNicheImageFrame = _welcomeLabel.frame;
    aboutNicheImageFrame.origin.y = self.view.bounds.size.height;
    
    [UIView animateWithDuration:0.75
                          delay:0.5
                        options:  UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self->_welcomeLabel.alpha = 1;
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];

}

- (void) transition {
    
    UIViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:login animated:false completion:nil];
    
}

@end
