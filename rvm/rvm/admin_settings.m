//
//  admin_settings.m
//  rvm
//
//  Created by Shane Daly on 05/03/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "admin_settings.h"

@interface admin_settings ()

@end

NSArray *name, *address, *number, *email, *gp_name, *gp_contact, *nok_name, *nok_contact;
NSString  *nameS, *addressS, *numberS, *emailS, *gp_nameS, *gp_contactS, *nok_nameS, *nok_contactS;

@implementation admin_settings

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initArray {
    name = @[@"Shane Daly", @"John Brown", @"Mary Green", @"Jenny Black", @"Gary Blue"];
    address = @[@"Cork", @"Kerry", @"Clare", @"Dublin", @"Waterford"];
    number = @[@"0870552746", @"0861234567", @"0871234567", @"0851234567", @"0891234567"];
    email = @[@"shane@cit.ie", @"john@cit.ie", @"mary@cit.ie", @"jenny@cit.ie", @"gary@cit.ie"];
    gp_name = @[@"Barry", @"Brian", @"Brendan", @"Bran", @"Buck"];
    gp_contact = @[@"087", @"086", @"087", @"085", @"089"];
    nok_name = @[@"Jess", @"Kerrie", @"Martin", @"Billy", @"Julie"];
    nok_contact = @[@"087", @"086", @"087", @"085", @"089"];
}

- (void) addSamples {
    int x = 0;
    
    for (x = 0; x < [name count]; x++) {
        nameS = [name objectAtIndex:x];
        addressS = [address objectAtIndex:x];
        numberS = [number objectAtIndex:x];
        emailS = [email objectAtIndex:x];
        gp_nameS = [gp_name objectAtIndex:x];
        gp_contactS = [gp_contact objectAtIndex:x];
        nok_nameS = [nok_name objectAtIndex:x];
        nok_contactS = [nok_contact objectAtIndex:x];
    }
}

- (void) addToCore {
    
}
@end
