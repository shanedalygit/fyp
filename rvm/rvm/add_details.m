//
//  add_details.m
//  rvm
//
//  Created by Shane Daly on 01/02/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "add_details.h"
#import <QuartzCore/QuartzCore.h>

@interface add_details ()

@end

NSString *patient_Name, *patient_Room, *filePath;
NSMutableArray *names, *rooms;
NSManagedObjectContext *context;

@implementation add_details

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    context = [appd managedObjectContext];
    self.admission_reason.layer.borderWidth = 1.0f;
    self.admission_reason.textColor = [UIColor lightGrayColor];
    _admission_reason.text = @"Enter Admission Reason..";
    self.admission_reason.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    names = [[NSMutableArray alloc]  init];
    rooms = [[NSMutableArray alloc]  init];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) returntoLogin {
    UIViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:dashboard animated:false completion:nil];
}

- (IBAction)add_patient:(id)sender {

    // Get a count for ID's
    
    NSArray *patientArray;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setReturnsObjectsAsFaults:NO];
    
    NSManagedObject *pat;
    pat = [NSEntityDescription insertNewObjectForEntityForName:@"Patient" inManagedObjectContext: context];
    NSError *error;
    patientArray = [context executeFetchRequest:request error:&error];
    [context save:&error];
    
    NSDateFormatter *date = [[NSDateFormatter alloc] init];
    [date setDateFormat:@"dd-MM-yyyy"];
    NSString *theDate = [date stringFromDate:[NSDate date]];
    
    // Get a random number in range of 0-5
    int bed_min = 1;
    int bed_max = 12;
    int rndValue = arc4random_uniform(bed_max - bed_min + 1) + bed_min;
    
    NSString *bedVals = [NSString stringWithFormat:@"%d", rndValue];
    
    int room_min = 0;
    int room_max = 100;
    int roomVal = arc4random_uniform(room_max - room_min + 1) + room_min;
    
    NSString *roomVals = [NSString stringWithFormat:@"%d", roomVal];
    
    NSString *x = [NSString stringWithFormat:@"%ld", ([patientArray count])+1];
    [pat setValue:_name_text.text forKey:@"name"];
    [pat setValue:roomVals forKey:@"room"];
    [pat setValue:bedVals forKey:@"bed"];
    [pat setValue:_dob_text.text forKey:@"dob"];
    [pat setValue:_address_text.text forKey:@"address"];
    [pat setValue:_contact_text.text forKey:@"contact"];
    [pat setValue:_email_text.text forKey:@"email"];
    [pat setValue:_gp_name.text forKey:@"gpname"];
    [pat setValue:_gp_contact.text forKey:@"gpcontact"];
    [pat setValue:x forKey:@"id"];
    [pat setValue:_nextof_kin.text forKey:@"next_of_kin"];
    [pat setValue:_kin_contact.text forKey:@"next_kin_contact"];
    [pat setValue:_admission_reason.text forKey:@"admit_reason"];
    [pat setValue:theDate forKey:@"admit_date"];
    [pat setValue:x forKey:@"perscription_id"];
    
    if([context save:&error] ) {
        NSLog(@"Saved successfully");
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Patient Added"
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self performSelector:@selector(returntoLogin)];
                                                         }];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark Keyboard Movement
- (void) dismissKeyboard {
    [self.view endEditing:YES];
}

- (void) textViewDidBeginEditing:(UITextView *) textView {
    self.admission_reason.text = @"";
    self.admission_reason.textColor = [UIColor blackColor];
}
@end
