//
//  LogEvent.h
//  rvm
//
//  Created by Shane Daly on 04/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface LogEvent : UIViewController <UITextViewDelegate>
{
    AppDelegate *appd;
}
@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UILabel *patientName_Log;
@property (weak, nonatomic) IBOutlet UITextView *note_Log;
@property (weak, nonatomic) IBOutlet UITextView *event_Log;
- (IBAction)submit_Note:(id)sender;
- (IBAction)submit_Event:(id)sender;

@end
