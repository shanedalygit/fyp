//
//  VitalSigns.m
//  rvm
//
//  Created by Shane Daly on 09/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "VitalSigns.h"
#import <HealthKit/HealthKit.h>

@interface VitalSigns ()

@end
int rate_choice = 0;

@implementation VitalSigns

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self solid_choice];
    [NSTimer scheduledTimerWithTimeInterval:3.0f
                                     target:self selector:@selector(simulateMetrics) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) simulateMetrics {
    NSArray *heartRateNormal = @[@"70", @"71", @"71", @"71", @"70", @"72", @"72", @"73", @"70", @"72", @"72", @"73", @"73"];
    NSArray *heartRateElevated = @[@"88",  @"88", @"88", @"89", @"89", @"89", @"89", @"90", @"90", @"90", @"90", @"90", @"90"];
    NSArray *heartRateDrop = @[@"60", @"55", @"56", @"57", @"59", @"61", @"60", @"60", @"60", @"55", @"55", @"55", @"56"];
    
    // Get a random number in range of 0-5
    int min = 0;
    int max = 12;
    int rndValue = arc4random_uniform(max - min + 1) + min;
    
    // set HR label to the value
    
    
    // Add Case where the type of metric is chosen - normal, elevated, drop
    switch(rate_choice) {
        case 0:
            _hr.text = (NSString *)heartRateNormal[rndValue];
            break;
        case 1:
           _hr.text = (NSString *)heartRateElevated[rndValue];
            break;
        case 2:
           _hr.text = (NSString *)heartRateDrop[rndValue];
            break;
        default:
           _hr.text = (NSString *)heartRateNormal[rndValue];
            break;
            
    }
}

- (void) solid_choice {
    int min = 0;
    int max = 2;
    int value = arc4random_uniform(max - min + 1) + min;
    
    switch(value) {
        case 0:
            rate_choice = value;
            break;
        case 1:
            rate_choice = value;
            break;
        case 2:
            rate_choice = value;
            break;
        default:
            rate_choice = 1;
            break;
            
    }
}
- (IBAction)back:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}
@end
