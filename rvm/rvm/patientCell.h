//
//  patientCell.h
//  rvm
//
//  Created by Shane Daly on 22/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface patientCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *patientName;
@property (weak, nonatomic) IBOutlet UILabel *patientRoom;
@property (weak, nonatomic) IBOutlet UILabel *patientStatus;


@end
