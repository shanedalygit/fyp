//
//  dashboard.m
//  rvm
//
//  Created by Shane Daly on 18/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import "dashboard.h"
#import "patientCustomCell.h"
#import "AppDelegate.h"

@interface dashboard ()

@end
@implementation dashboard {
    
    NSMutableArray *patients;
    NSMutableArray *rooms;
    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    patientCustomCell *cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    context = [appd managedObjectContext];
    entityDesc = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:context];
    
    self.administrator_settings.tintColor = [UIColor clearColor];
    self.demo_data.tintColor = [UIColor clearColor];
    
    [self viewDidAppear:YES];
}

- (void) viewDidAppear:(BOOL)animated {
    [self populateCells];
    [NSTimer scheduledTimerWithTimeInterval:2.0f
                                     target:self selector:@selector(checkReloads) userInfo:nil repeats:YES];
}

- (void) checkReloads {
    [self.tableView reloadData];
}

-(void) checkUsers {
    
    // Get the passed ID.
    NSString *userID = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"id"];
//    // Get the
//    NSEntityDescription *adminEntity = [NSEntityDescription entityForName:@"Users" inManagedObjectContext:context];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    NSPredicate *check = [NSPredicate predicateWithFormat:@"(id = %@)", userID];
//    [request setEntity:adminEntity];
//    [request setPredicate:check];
//    [request setReturnsObjectsAsFaults:NO];
      NSError *error = nil;
//    NSArray *creds = [context executeFetchRequest:request error:&error];
//    NSMutableArray *staffID = [creds valueForKey:@"id"];
//    NSString *compare = staffID[0];
    
    NSEntityDescription *checkEn = [NSEntityDescription entityForName:@"Users" inManagedObjectContext:context];
    NSFetchRequest *enRequest = [[NSFetchRequest alloc] init];
    NSPredicate *enPred = [NSPredicate predicateWithFormat:@"(id = %@)", userID];
    [enRequest setEntity:checkEn];
    [enRequest setPredicate:enPred];
    [enRequest setReturnsObjectsAsFaults:NO];
    NSArray *checking = [context executeFetchRequest:enRequest error:&error];
    NSMutableArray *adminID = [checking valueForKey:@"isAdmin"];
    
    NSString *confirmation = adminID[0];
    
    if (![confirmation isEqualToString:@"1"]) {
        [self.demo_data setEnabled:NO];
        [self.demo_data setTintColor: [UIColor clearColor]];
        
        [self.administrator_settings setEnabled:NO];
        [self.administrator_settings setTintColor: [UIColor clearColor]];
    }
}


- (void) populateCells {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setReturnsObjectsAsFaults:NO];
    NSError *error = nil;
    NSArray *patient = [context executeFetchRequest:request error:&error];
    rooms = [patient valueForKey:@"room"];
    patients = [patient valueForKey:@"name"];
    

    
    // Initialize the arrays.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [patients count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    // Configure the cell...
    static NSString *simpleTableID = @"patientCell";
    
    cell = (patientCustomCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableID];
    
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"customCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.patientName.text = [patients objectAtIndex:indexPath.row];
    cell.patientRoom.text = [rooms objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.patientName.text = [patients objectAtIndex:indexPath.row];
    NSString *toPass = cell.patientName.text;
    long value = indexPath.row;
    [[NSUserDefaults standardUserDefaults] setObject:toPass forKey:@"name"];
    [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"edit_me"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self performSegueWithIdentifier:@"PatientDetails" sender:tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}

- (void)setCellState {
    
}

@end
