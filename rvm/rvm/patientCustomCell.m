//
//  patientCustomCell.m
//  rvm
//
//  Created by Shane Daly on 22/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import "patientCustomCell.h"

@implementation patientCustomCell

@synthesize patientName = _patientName;
@synthesize patientRoom = _patientRoom;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
