//
//  addUsers.m
//  rvm
//
//  Created by Shane Daly on 04/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "addUsers.h"

@interface addUsers ()

@end
NSEntityDescription *user_entityDesc;
NSManagedObjectContext *user_ManObj;
Boolean agreement;
NSString *admin;

@implementation addUsers
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    user_ManObj = [appd managedObjectContext];
    user_entityDesc = [NSEntityDescription entityForName:@"Users" inManagedObjectContext:user_ManObj];
    
    // Dismiss Keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    // Disable Buttons
    _addUserButton.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboard {
    [self.view endEditing:true];
}

- (IBAction)create:(id)sender {
    
    if (agreement == true) {
    NSEntityDescription *entityDescrip = [NSEntityDescription entityForName:@"Users" inManagedObjectContext:user_ManObj];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescrip];
    
    NSManagedObject *user;
    user = [NSEntityDescription insertNewObjectForEntityForName:@"Users" inManagedObjectContext: user_ManObj];
    NSError *error;
    [user_ManObj save:&error];
    
    [user setValue:_thenew_username.text forKey:@"username"];
    [user setValue:_thenew_password.text forKey:@"password"];
    [user setValue:_staff_id.text forKey:@"staff_id"];

    if([user_ManObj save:&error] ) {
    
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Account Created"
                                                                                 message:@"Please login using the credentials."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self performSelector:@selector(returntoLogin)];
                                                         }];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
}

- (void) returntoLogin {
    UIViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:dashboard animated:false completion:nil];
}


- (IBAction)iAgree:(id)sender {
    agreement = TRUE;
    _addUserButton.enabled = YES;
}

- (IBAction)iDisagree:(id)sender {
    agreement = FALSE;
    _addUserButton.enabled = NO;
}

- (IBAction)return:(id)sender {
    [self returntoLogin];
}
@end
