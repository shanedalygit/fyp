//
//  patientCustomCell.h
//  rvm
//
//  Created by Shane Daly on 22/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface patientCustomCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *patientName;
@property (nonatomic, weak) IBOutlet UILabel *patientRoom;


@end
