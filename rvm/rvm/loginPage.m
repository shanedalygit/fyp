//
//  loginPage.m
//  rvm
//
//  Created by Shane Daly on 15/01/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "loginPage.h"
#import "AppDelegate.h"

@interface loginPage ()

@end

NSString *username, *password, *compare_user, *compare_pass;
NSArray *loginDetails;
NSMutableArray *user, *pass, *staff_id;
NSEntityDescription *entity;
NSManagedObjectContext *manObject;
NSPredicate *predicate;

@implementation loginPage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Set the error label to blank.
    _login_status.text = @"";
    
    // Set up Core Access.
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    manObject = [appd managedObjectContext];
    entity = [NSEntityDescription entityForName:@"Users" inManagedObjectContext:manObject];
    
    // Keyboard Dismissal Gesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void) dismissKeyboard {
    [self.view endEditing:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) login:(UIButton *)sender {
    
    username = _username_Field.text;
    password = _password_Field.text;
    //staff_id = _staff_Field.text;
    
    predicate = [NSPredicate predicateWithFormat:@"(name = %@)", username];
    [self getCredentials];
    
    if ([self checkDetails] == true) {
        [self passedCheck];
    }
    
}

- (IBAction)new_user:(id)sender {
    
    UIViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"new_user"];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:dashboard animated:false completion:nil];
}

- (IBAction)forgot_id:(id)sender {
}

- (void) getCredentials {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setReturnsObjectsAsFaults:NO];
    //[request setPredicate:predicate];
    NSError *error = nil;
    loginDetails = [manObject executeFetchRequest:request error:&error];
    
    user = [loginDetails valueForKey:@"username"];
    pass = [loginDetails valueForKey:@"password"];
    staff_id = [loginDetails valueForKey:@"staff_id"];
    
    for (int x = 0; x < [user count]; x++) {
        compare_user = [user objectAtIndex:x];
        compare_pass = [pass objectAtIndex:x];
        [self checkDetails];
    }
    
}

- (BOOL) checkDetails {

    if (_username_Field.hasText && _password_Field.hasText) {
        if (username == compare_user && password == compare_pass) {
            [[NSUserDefaults standardUserDefaults] setObject:staff_id[0] forKey:@"staff_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            return true;
        } else {
             _login_status.text = @"Invalid Login Details. Please Try Again.";
        }
    } else if (_username_Field.hasText && (_password_Field.text == nil)) {
        _login_status.text = @"Please enter your password";
    } else if ((_username_Field.text == nil) && _password_Field.hasText) {
         _login_status.text = @"Please enter your username";
    } else {
        _login_status.text = @"Please enter your details.";
    }
    return false;
}

- (void) passedCheck {
    
    UIViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:dashboard animated:false completion:nil];
}


// Testing
#pragma mark Keyboard Scroll
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterFromKeyboardNotifications];
    [super viewWillDisappear:animated];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGPoint buttonOrigin = self.loginButton.frame.origin;
    CGFloat buttonHeight = self.loginButton.frame.size.height;
    CGRect visibleRect = self.view.frame;
    visibleRect.size.height -= keyboardSize.height;
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

@end
