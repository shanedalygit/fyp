//
//  historyCell.m
//  rvm
//
//  Created by Shane Daly on 05/04/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "historyCell.h"

@interface historyCell ()


@end

@implementation historyCell

@synthesize date = _date;
@synthesize description = _description;

- (void) awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
