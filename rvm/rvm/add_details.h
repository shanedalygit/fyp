//
//  add_details.h
//  rvm
//
//  Created by Shane Daly on 01/02/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface add_details : UIViewController <UITextViewDelegate>
{
     AppDelegate *appd;
}
@property (strong, nonatomic) NSMutableArray *name_Array;
@property (strong, nonatomic) NSMutableArray *room_Array;
@property (weak, nonatomic) IBOutlet UITextField *name_text;
@property (weak, nonatomic) IBOutlet UITextField *room_text;
@property (weak, nonatomic) IBOutlet UITextField *dob_text;
@property (weak, nonatomic) IBOutlet UITextField *address_text;
@property (weak, nonatomic) IBOutlet UITextField *contact_text;
@property (weak, nonatomic) IBOutlet UITextField *email_text;
@property (weak, nonatomic) IBOutlet UITextField *gp_name;
@property (weak, nonatomic) IBOutlet UITextField *gp_contact;
@property (weak, nonatomic) IBOutlet UITextField *bed_text;
@property (weak, nonatomic) IBOutlet UITextField *nextof_kin;
@property (weak, nonatomic) IBOutlet UITextField *kin_contact;
@property (weak, nonatomic) IBOutlet UITextView *admission_reason;
@property (weak, nonatomic) IBOutlet UIButton *add_button;

- (IBAction)add_patient:(id)sender;
@property(nonatomic,retain)NSManagedObjectContext *managedObjectContext;

@end
