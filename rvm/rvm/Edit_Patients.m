//
//  Edit_Patients.m
//  rvm
//
//  Created by Shane Daly on 14/03/2018.
//  Copyright © 2018 Shane Daly. All rights reserved.
//

#import "Edit_Patients.h"
#import "AppDelegate.h"

@interface Edit_Patients ()

@end

NSArray *edit_patient;
NSMutableArray *edit_profile_details, *edit_rooms, *edit_dobs, *edit_address, *edit_contacts, *edit_emails, *edit_gpnames, *edit_gpcontacts, *edit_bed, *edit_kin, *edit_k_contact;
NSEntityDescription *edit_entityDesc;
NSManagedObjectContext *edit_manObj;
NSPredicate *edit_pred;
NSString *edit_profile;

@implementation Edit_Patients

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Instantiate the CoreData fuctions from the AppDelegate.
    appd=(AppDelegate *) [[UIApplication sharedApplication]delegate];
    edit_manObj = [appd managedObjectContext];
    edit_entityDesc = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:edit_manObj];
    
    self.history_text.layer.borderWidth = 1.5f;
    self.history_text.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self getValues];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getValues {
    // Get the name, set the predicate.
    edit_profile = [[NSUserDefaults standardUserDefaults] stringForKey:@"name"];
    edit_pred = [NSPredicate predicateWithFormat:@"(name = %@)", edit_profile];
   
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:edit_entityDesc];
    [request setReturnsObjectsAsFaults:NO];
    [request setPredicate:edit_pred];
    NSError *error = nil;
    edit_patient = [edit_manObj executeFetchRequest:request error:&error];
    
    edit_rooms = [edit_patient valueForKey:@"room"];
    edit_dobs = [edit_patient valueForKey:@"dob"];
    edit_address = [edit_patient valueForKey:@"address"];
    edit_contacts = [edit_patient valueForKey:@"contact"];
    edit_emails = [edit_patient valueForKey:@"email"];
    edit_gpnames = [edit_patient valueForKey:@"gpname"];
    edit_gpcontacts = [edit_patient valueForKey:@"gpcontact"];
    edit_kin = [edit_patient valueForKey:@"next_of_kin"];
    edit_k_contact = [edit_patient valueForKey:@"next_kin_contact"];
    
    _name_text.text = edit_profile;
    _room_text.text = [edit_rooms objectAtIndex:0];
    _dob_text.text = [edit_dobs objectAtIndex:0];
    _address_text.text = [edit_address objectAtIndex:0];
    _contact_text.text = [edit_contacts objectAtIndex:0];
    _email_text.text = [edit_emails objectAtIndex:0];
    _gp_names.text = [edit_gpnames objectAtIndex:0];
    _gp_contacts.text = [edit_gpcontacts objectAtIndex:0];
    _next_of_kin_name.text = [edit_kin objectAtIndex:0];
    _next_of_kin_no.text = [edit_k_contact objectAtIndex:0];
}

- (IBAction)done:(id)sender {
    
    // Send the updated values.
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Patient" inManagedObjectContext:edit_manObj]];
    
    // Get the Correct Row
    long long int row = [[[NSUserDefaults standardUserDefaults] objectForKey:@"edit_me"] longLongValue];
    
    NSError *error = nil;
    NSArray *results = [edit_manObj executeFetchRequest:request error:&error];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", edit_profile];
    [request setPredicate:predicate];
    
    NSManagedObject* patients_result = [results objectAtIndex:row];
    [patients_result setValue:_name_text.text forKey:@"name"];
    [patients_result setValue:_room_text.text forKey:@"room"];
    [patients_result setValue:_dob_text.text forKey:@"dob"];
    [patients_result setValue:_address_text.text forKey:@"address"];
    [patients_result setValue:_contact_text.text forKey:@"contact"];
    [patients_result setValue:_email_text.text forKey:@"email"];
    [patients_result setValue:_gp_names.text forKey:@"gpname"];
    [patients_result setValue:_gp_contacts.text forKey:@"gpcontact"];
    [patients_result setValue:_next_of_kin_name.text forKey:@"next_of_kin"];
    [patients_result setValue:_next_of_kin_no.text forKey:@"next_kin_contact"];

    
    if([edit_manObj save:&error]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Account Created"
                                                                                 message:@"Patient Updated."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self performSelector:@selector(returnToDash)];
                                                         }];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)delete:(id)sender {
    // Delete the selected patient.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:edit_manObj];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@",edit_profile];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *items = [edit_manObj executeFetchRequest:fetchRequest error:&error];
    
    //NSMutableArray *dob = [items valueForKey:@"dob"];

    for (NSManagedObject *managedObject in items) {
                [edit_manObj deleteObject:managedObject];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Patient Deleted"
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self performSelector:@selector(returnToDash)];
                                                         }];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void) returnToDash {
    UIViewController *dashboard = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:dashboard animated:false completion:nil];
}
@end
