//
//  welcomeViewController.h
//  rvm
//
//  Created by Shane Daly on 17/09/2017.
//  Copyright © 2017 Shane Daly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface welcomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoFade;

@end
